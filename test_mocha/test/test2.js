const assert = require('chai').assert;
const app = require('../app2');

describe('App2', function(){

    it('app2 should return 3 ', function(){
        assert.equal(app(), 3);
    });

    it('app2 should return a number', function(){
        assert.typeOf(app(), 'number');
    });
});