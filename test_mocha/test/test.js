const assert = require('chai').assert;
const app = require('../app');

describe('App', function(){

    it('app should return "hello" ', function(){
        assert.equal(app(), 'hello');
    });

    it('app should return a string', function(){
        assert.typeOf(app(), 'string');
    });
});