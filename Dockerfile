# This file is a template, and might need editing before it works on your project.
FROM node:7.9

WORKDIR sauna-project/app/

ARG NODE_ENV
ENV NODE_ENV $NODE_ENV
ADD https://gitlab.com/casette/sauna-project/raw/master/app/package.json .
RUN npm install
COPY . .

CMD node index.js && node main.js

# replace this with your application's default port
EXPOSE 3000
EXPOSE 80
