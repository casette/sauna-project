*** Settings ***
Library	SeleniumLibrary
Library	XvfbRobot

*** Variables ***
${SERVER}				http://127.0.0.1
${BROWSER}				firefox
${DELAY}				0
${USER}					settiboi
${PASSWORD}				settiboi
${CHANGED_PASSWORD}		settiboi1
${EMAIL}				settiboi@info.net

*** Keywords ***
Open Browser To Page
	[Arguments]				${ADDRESS}
	Start Virtual Display	1920		1080
	Open Browser			${ADDRESS}	${BROWSER}
	Set Selenium Speed		${DELAY}
	Set Window Size			1920		1080

Open Browser To Main Page
	Start Virtual Display	1920		1080
	Open Browser			${SERVER}	${BROWSER}
	Set Selenium Speed		${DELAY}
	Set Window Size			1920		1080

Login
	Input Text		name:username	${USER}
	Input Password	name:password	${PASSWORD}
	Click Button 	name:login_button
	Wait Until Keyword Succeeds 	10 sec  2 sec   Title Should Be		Dashboard

Login As
	[Arguments]		${ARG_USERNAME}		${ARG_PASSWORD}
	Input Text		name:username		${ARG_USERNAME}
	Input Password	name:password		${ARG_PASSWORD}
	Click Button 	name:login_button
	Wait Until Keyword Succeeds 	10 sec  2 sec   Title Should Be		Dashboard

Logout
	Click Link 	name:logout_link
	Wait Until Keyword Succeeds		10 sec	2 sec	Title Should Be		Sauna.io

Input Credentials
	[Arguments]		${ARG_USERNAME}		${ARG_PASSWORD}
	Input Text		name:username		${ARG_USERNAME}
	Input Password	name:password		${ARG_PASSWORD}

Navigate
	[Arguments]		${ARG_BARPART}		${ARG_NAVBOX}

Go To Settings
	Click Link		id:settings

Change Password
	[Arguments]		${ARG_CURRENT_PW}	${ARG_NEW_PW}
	Input Password	name:password		${ARG_CURRENT_PW}
	Input Password	name:newPassword	${ARG_NEW_PW}
	Input Password	name:passwordConf	${ARG_NEW_PW}
	Click Button 	name:password_button
	Wait Until Keyword Succeeds		10 sec	2 sec	Element Should Contain	name:password_result	Success

Change Email
	[Arguments]		${ARG_EMAIL}		${ARG_PASSWORD}
	Input Text		name:email			${ARG_EMAIL}
	Input Password	id:emailPassword	${ARG_PASSWORD}
	Click Button	name:email_button
	Wait Until Keyword Succeeds		10 sec	2 sec	Element Should Contain	name:email_result	Success
