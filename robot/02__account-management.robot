*** Settings ***
Resource		resources.robot

*** Test Cases ***
Change Password
    [Setup]     Open Browser To Main Page
    Login
    Go To Settings
    Change Password  ${PASSWORD}  ${CHANGED_PASSWORD}
    Logout
    Login As  ${USER}  ${CHANGED_PASSWORD}
    Go To Settings
    Change Password  ${CHANGED_PASSWORD}  ${PASSWORD}
    Logout
    [Teardown]  Close Browser

Change Email
    [Setup]     Open Browser To Main Page
    Login
    Go To Settings
    Change Email  ${EMAIL}  ${PASSWORD}
    Logout
    [Teardown]  Close Browser
    