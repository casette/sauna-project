*** Settings ***
Resource		resources.robot
Default Tags	smoke

*** Test Cases ***
Login and Logout
	[Setup]     Open Browser To Main Page
	Login
	Logout
	[Teardown]	Close Browser

Block Login With Wrong Credentials
	[Setup]     Open Browser To Main Page
	Input Credentials	sëttïböï	sëttïböï
	Sleep	10s
	Title Should Be		Sauna.io
	[Teardown]	Close Browser
