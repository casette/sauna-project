var express = require('express');
var app     = express();
var port    = process.env.PORT || 80;

app.use(express.static('html'));
app.get("/", function(req, res) {
    res.sendFile(__dirname + "/" + "login-testi.html");
})

app.listen(port);
