var MongoClient = require('mongodb').MongoClient;
var express = require('express');
var app = express();
var dbConfig = require('./config/database.js');
var session = require('express-session');
var bodyParser  = require('body-parser');
var url = dbConfig.url;
var addData = express.Router();
var mongoSanitize = require('express-mongo-sanitize');

const MongoStore = require('connect-mongo')(session);

const { createLogger, format, transports }  = require('winston');
const { combine, timestamp, label, printf } = format;

const myFormat = printf(info => {
    return `${info.timestamp} [${info.label}] ${info.level}: ${info.message}`;
  });
const logger = createLogger({
  level: 'info',
  format: format.combine(
      label({label:'Dev'}),
      timestamp(),
      myFormat
    ),
  transports: [
    //
    // - Write to all logs with level `info` and below to `combined.log'
    // - Write all logs error (and below) to `error.log`.
    //
    new transports.File({ filename: 'logs/error.log', level: 'error', 'timestamp':true }),
    new transports.File({ filename: 'logs/combined.log', 'timestamp':true })
  ]
});

addData.use(bodyParser.urlencoded({ extended: true }));
addData.use(bodyParser.json());
addData.use(mongoSanitize());

addData.post('/devicedata', function (req, res){

    MongoClient.connect(url, function (dbErr, db){
        if(dbErr){
            res.json({"status":"error","error":"couldn't connect to database"});
        }
        var device = req.body.deviceName;
        var type = req.body.type;
        var value = req.body.value;

        var deviceCollection = db.collection("Devices");
        var valueCollection = db.collection("Values");
        deviceCollection.findOne({deviceName:device},{_id:0}, function(err, result){

            //console.log(result);
            //logger.info(result);
            if (err){
                console.log(err);
                logger.error(err);
            }
            if(result == null){
                console.log("Device does not exist");
                logger.info("Device does not exist");
                res.json({"status":"error","error":"device does not exist"});
            }
            else {
                valueCollection.insert(
                    {
                        type: type,
                        value: value,
                        time: new Date(Date.now()),
                        deviceName: device
                    }
                    ,function(err, result){
                        if(err){
                            logger.error(err);
                            console.log(err);
                        }
                        db.close();
                    })
            }
        })
    })
})

module.exports = addData;
