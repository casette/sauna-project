var MongoClient = require('mongodb').MongoClient;
var express = require('express');
var app = express();
var dbConfig = require('./config/database.js');
var session = require('express-session');
var bodyParser  = require('body-parser');
var url = dbConfig.url;
var register = express.Router();
var mongoSanitize = require('express-mongo-sanitize');

const MongoStore = require('connect-mongo')(session);

const { createLogger, format, transports }  = require('winston');
const { combine, timestamp, label, printf } = format;

const myFormat = printf(info => {
    return `${info.timestamp} [${info.label}] ${info.level}: ${info.message}`;
  });
const logger = createLogger({
  level: 'info',
  format: format.combine(
      label({label:'Dev'}),
      timestamp(),
      myFormat
    ),
  transports: [
    //
    // - Write to all logs with level `info` and below to `combined.log` 
    // - Write all logs error (and below) to `error.log`.
    //
    new transports.File({ filename: 'logs/error.log', level: 'error', 'timestamp':true }),
    new transports.File({ filename: 'logs/combined.log', 'timestamp':true })
  ]
});

register.use(bodyParser.urlencoded({ extended: true }));
register.use(bodyParser.json());
register.use(mongoSanitize());

register.post('/add-device', function (req, res){

    MongoClient.connect(url, function (dbErr, db){
        if(dbErr){
            res.json({"status":"error","error":"couldn't connect to database"});
        }
        var device = req.body.deviceName;
        var deviceCollection = db.collection("Devices");
        deviceCollection.findOne({deviceName:device},{_id:0}, function(err, result){

            //console.log(result);
            //logger.info(result);
            if (err){
                console.log(err);
                logger.error(err);
            }
            if(result == null){
                deviceCollection.insert(
                    {
                        deviceName: device,
                        timeDeviceAdded: new Date(Date.now())
                    }
                    ,function(err, result){
                        if(err){
                            console.log(err);
                            logger.error(err);
                        }
                        db.close();
                    }
                )
                console.log("1 device inserted to database");
                logger.info("1 device inserted to database");
                res.json({"status":"success"});
            }
            else {
                res.json({"status":"error","error":"device already exists"});
            }
        })
    })
})

register.post('/add-data', function (req, res){

    MongoClient.connect(url, function (dbErr, db){
        if(dbErr){
            res.json({"status":"error","error":"couldn't connect to database"});
        }
        var device = req.body.deviceName;
        var type = req.body.type;
        var celciusValue = req.body.CelciusTemp;
        var kelvinValue = req.body.KelvinTemp;

        celciusValue = parseFloat(celciusValue);
        kelvinValue = parseFloat(kelvinValue);

        var deviceCollection = db.collection("Devices");
        var valueCollection = db.collection("Values");
        deviceCollection.findOne({deviceName:device},{_id:0}, function(err, result){

            //console.log(result);
            //logger.info(result);
            if (err){
                console.log(err);
                logger.error(err);
            }
            if(result == null){
                console.log("Device does not exist");
                logger.info("Device does not exist");
                res.json({"status":"error","error":"device does not exist"});
            }
            else {
                valueCollection.insertMany(
                    [
                        {
                            type: "CelciusTemp",
                            value: celciusValue,
                            time: new Date(Date.now()),
                            device: device
                        },
                        {
                            type: "KelvinTemp",
                            value: kelvinValue,
                            time: new Date(Date.now()),
                            device: device
                        }
                    ],
                    function(err, result){
                        if(err){
                            logger.error(err);
                            console.log(err);
                        }
                        db.close();
                    }
                )
                res.json({"status":"success"});
            }
        })
    })
})

register.get('/all-devices', function(req,res) {
    MongoClient.connect(url, function(err, db){
        if(err){
            res.json({"status":"error","error":"couldn't connect to database"});
        }
        var res_json = [];
        var stream = db.collection("Devices").find({},{_id:0,deviceName:1});
        stream.on("data", function(value) {
            res_json.push(value);
        });
        stream.on("end", function() {
            db.close();
            res.json(res_json);
        });
    });
})

module.exports = register;
