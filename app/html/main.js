//src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"
//src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"
var json_obj;
var i, x = "";
var n;
var saunaData;
var labelData;
var kelvinData;
var labelKelvinData; 
var celcData;
var celcTimeData;
var loop;
var userName;
var deviceName;
var kelvData;
var kelvTimeData;
var img = new Image();
var jsonscript

img.src = '/css/tulta.png';

img.onload = function() {
	
	function loop(){
		
		$.getJSON( "/api/status", function(json2) {
			
			userName = json2.name;
			
			$.getJSON( "/api/user/" + userName, function(json3) {
				
				for (n in json3.devices) {
					
					deviceName = json3.devices[n];
					
					eitoimi(n, deviceName);
					
				}
				function eitoimi(luku, deviceName){
					$.getJSON( "/api/device/" + deviceName + "/values?count=20", function(json) {
						
						if(json.length != 0){							
							saunaData = new Array();
							labelData = new Array();
							kelvinData = new Array();
							labelKelvinData = new Array(); 
							celcData = new Array(); 
							kelvData = new Array();
							celcTimeData = new Array(); 
							kelvTimeData = new Array();
		
							for (i in json) {
								if(json[i].type=="CelciusTemp"){
								celcData.push(json[i].value);
								celcTimeData.push(json[i].time.slice(11, -5));
								}
							}	
		
							for (i in json) {
								if(json[i].type=="KelvinTemp"){
								kelvData.push(json[i].value);
								kelvTimeData.push(json[i].time.slice(11, -5));
								}
							}
		
							for (var b = 10; b > 0; --b) {
							saunaData.push(celcData[celcData.length-b]);
							labelData.push(celcTimeData[celcTimeData.length-b]);
							kelvinData.push(kelvData[kelvData.length-b]);
							labelKelvinData.push(kelvTimeData[kelvTimeData.length-b]);
							}
							
							document.getElementById("temperature" + luku + "_1").innerText = "Lämpötila: " + celcData[celcData.length-1] + " °C";
			
							document.getElementById("temperature" + luku + "_2").innerText = "Lämpötila: " + kelvData[kelvData.length-1] + " K";
							
							document.getElementById("chartdiv" + luku).style.border = "thick solid #000000";
							
							document.getElementById("chartdiv" + luku).style.display = "block";
							
					
							var ctx = document.getElementById("myChart"+ luku + "_1").getContext("2d");
							var fillPattern = ctx.createPattern(img, 'repeat');
							var myCelciusChart = new Chart(ctx, {
								type: 'line',
								data: {
									labels: labelData,
									lineTension: 0.1,
									datasets: [{
										label: deviceName + ': lämpötila Celciuksena',
										data: saunaData,
										backgroundColor: fillPattern,
										borderColor: 'rgb(115, 115, 115)',
										borderWidth: 3
									}]
								},
								options: {
									scales: {
										yAxes: [{
											ticks: {
												beginAtZero:true,
												max: 120
											}
										}]
							
									},
									tooltips: {
										enabled: false
									}
								}
							});
			
							var ctx2 = document.getElementById("myChart"+ luku + "_2").getContext("2d");
							var fillPattern = ctx2.createPattern(img, 'repeat');
							var myKelvinChart = new Chart(ctx2, {
								type: 'line',
								data: {
									labels: labelKelvinData,
									lineTension: 0.1,
									datasets: [{
										label: deviceName +': lämpötila Kelvineinä',
										data: kelvinData,
						
										backgroundColor: fillPattern,
										borderColor: 'rgb(115, 115, 115)',
										borderWidth: 3
									}]
								},
								options: {
									scales: {
										yAxes: [{
											ticks: {
												beginAtZero:true						
											}
										}]
									},
									tooltips: {
									enabled: false
									}
								}
							});
						}
					});
				}
			});
		});
		setTimeout(20000);
	}
	setInterval(loop, 20000);
	loop();
}