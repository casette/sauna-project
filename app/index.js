console.log('ja lähtee')
var ttn = require("ttn")
var conv = require('binstring') //convert hex
var fs = require('fs') //for writing to log.txt
var MongoClient = require('mongodb').MongoClient;
var tiedot = '';
var time = '';
var type = '';
var dbConfig = require('./config/database.js');

var f = require('util').format;
var assert = require('assert');
var winston = require('winston');

var url = dbConfig.url;

var appID = "casettesauna"
var accessKey = "ttn-account-v2.lDQVI5grIb6Vedkolb0w9iw6B4NfjYV_szMPUyVYHZA"

const logger = winston.createLogger({
  level: 'info',
  format: winston.format.json(),
  transports: [
    //
    // - Write to all logs with level `info` and below to `combined.log` 
    // - Write all logs error (and below) to `error.log`.
    //
    new winston.transports.File({ filename: 'logs/error.log', level: 'error', 'timestamp': true }),
    new winston.transports.File({ filename: 'logs/combined.log', 'timestamp': true })
  ]
});

ttn.data(appID, accessKey)
  .then(function (client) {
    client.on("uplink", function (devID, payload) {

	console.log("\nReceived uplink from ", devID)
    logger.info("\nReceived uplink from ", devID)
      //console.log(payload)
	time = payload.metadata.time;
	console.log('Time: ' + time)
	tiedot = payload.payload_raw
	type = tiedot.slice(0,1)
	type = conv(type,{out:'hex'});
	tiedot = conv(tiedot,{out:'hex'});
//IF TEMPERATURE SENSOR
	if(type == 11){
        
	   var data = [];
	   data = code11(tiedot)
	   console.log('Payload: ' + tiedot);
        logger.info('Payload: ' + tiedot);
        // Connecting to Database
	   MongoClient.connect(url, function(err, db){
           
           assert.equal(null, err);
	       console.log('Connected to Database!');
           logger.info('Connected to Database!');
           var collection = db.collection("Values");
           var deviceCollection = db.collection("Devices");

           collection.insertMany(
               [
                   {
                       type: "CelciusTemp",
                       value: data[0],
                       time: new Date(time),
                       device: payload.dev_id
                   },
                   {        
                       type: "KelvinTemp",
                       value: data[1],
                       time: new Date(time),
                       device: payload.dev_id
                   }                       
               ],
               function(err, result){
                   if(err){
                       logger.error(err);
                   }
               });
           
           deviceCollection.findOne({deviceName:payload.dev_id},{_id: 0}, function(err, result){
               
               console.log(result);
               if (err){
                   logger.error(err);
               }
               
               if(result == null){
                   deviceCollection.insert(
                       {
                           deviceName: payload.dev_id
                       }
                       ,function(err, result){
                           if(err){
                               logger.error(err);
                               console.log(err);
                           }
                           db.close();
                       })
               }
               
             
           }) 
           console.log("1 entry inserted to database");
           logger.info("1 entry inserted to database");
       })
    };
//IF DATA == 11 ENDS
    })
})
    .catch(function (error) {
    console.error("Error", error)
    process.exit(1)
});


function code11(tiedot){

    /* CODE 11 FUNCTION
    This function returns data array[float]
    First index of data array is second and third byte of the payload
    If data = 11 22 33 44 55 then data[0] = 22.33 (Converted from hex to decimal)

    Second index of data is the fourth and fifth byte combined
    data[1] = 4455 (Converted from hex to decimal)

    First byte of the data only determines what type of data it is
    This function runs if first byte is 11 (for now)
    */
    
	var celcius = parseInt(tiedot.slice(2,4), 16);
	var decimal = parseInt(tiedot.slice(4,6), 16);
	var kelvin = parseInt(tiedot.slice(6), 16);
    
  	logger.info("Temp: " + celcius + "." + decimal + "C " + kelvin + "K"); 

	celcius = celcius.toString();
	var piste = '.';
	decimal = decimal.toString();


	var luku = celcius.concat(piste, decimal);
	luku = parseFloat(luku);
	kelvin = parseFloat(kelvin);
	var arr = [];
	arr.push(luku);
	arr.push(kelvin);
	return arr;
}

//kommentti
