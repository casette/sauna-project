var MongoClient = require('mongodb').MongoClient;
var assert      = require('assert');
var express     = require('express');
var app         = express();
var rest_api    = express.Router();
var dbConfig    = require('./config/database.js');
var session     = require('express-session');
var bodyParser  = require('body-parser');
var bcrypt      = require('bcrypt');
var cors        = require('cors');
var mongoSanitize = require('express-mongo-sanitize');

var url = dbConfig.url;

const MongoStore = require('connect-mongo')(session);
const saltRounds = 10;

const { createLogger, format, transports }  = require('winston');
const { combine, timestamp, label, printf } = format;

const myFormat = printf(info => {
    return `${info.timestamp} [${info.label}] ${info.level}: ${info.message}`;
  });
const logger = createLogger({
  level: 'info',
  format: format.combine(
      label({label:'Rest API'}),
      timestamp(),
      myFormat
    ),
  transports: [
    //
    // - Write to all logs with level `info` and below to `combined.log` 
    // - Write all logs error (and below) to `error.log`.
    //
    new transports.File({ filename: 'logs/error.log', level: 'error', 'timestamp':true }),
    new transports.File({ filename: 'logs/combined.log', 'timestamp':true })
  ]
});

// Exposes our API! Consider removing once developed!
rest_api.use(cors());

rest_api.use(session({
    secret: 'falafel',
    resave: false,
    unset: 'destroy',
    saveUninitialized: false,
    store: new MongoStore({ url: url })
}));

rest_api.use(bodyParser.urlencoded({ extended: true }));
rest_api.use(bodyParser.json()); 
rest_api.use(mongoSanitize());

/**
 * Used by various searches to reduce overall code length
 * @param {string} col - Database collection
 * @param {string} field - Field to be searched for
 * @param {string} id - User/Device/Set -name
 * @param {function} callback - Callback function
 */
function dbSearch(col, field, id, callback) {
    MongoClient.connect(url, function(err, db) {
        if (err) {
            logger.error(err);
        }

        var query = {};
        query[field] = id;
        var collection = db.collection(col);
        collection.findOne(query,{_id:0}, function(err, result) {
            if (err) {
                logger.error(err);
            }
            db.close();
            if (result !== null) {
                callback(result);
            }
            else {
                callback({"status":"error","error":"Target not found in " + col});
            }            
        });
    });
}


/* * * * * * * * * */
/* Account routes  */
/* * * * * * * * * */

// #region Account JS
rest_api.get('/logout', function(req, res) {
    if(req.session.name) {
        var name = req.session.name;
        MongoClient.connect(url, function(err, db) {
            if (err) {
                logger.error(err);
                res.json({"status":"error","error":"couldn't connect to database"});
            }
            db.collection('Users').findOneAndUpdate({userName:name},{$currentDate:{timeLastSignIn:true}});
        })
    }
    req.session.destroy(function(err) {
        if (err) {
            res.json({"status":"error","error":"logout failed for unknown reason"});
        }
        else {
            res.json({"status":"success"});
        }
    })
});

rest_api.post('/login', function(req, res) {
    if (req.session.name) {
        res.json({"status":"error","error":"already logged in"});
    }
    else if (req.body.username && req.body.password) {
        MongoClient.connect(url, function(dbErr, db) {
            if(dbErr) {
                logger.error(dbErr);
                res.json({"status":"error","error":"couldn't connect to database"});
            }

            var username = req.body.username;
            var password = req.body.password;

            var col = 'Users';
            var collection = db.collection(col);
            collection.findOne({userName:username}, function(dbErr2, result) {
                if (dbErr2) {
                    logger.error(dbErr2);
                }
                db.close();
                if (result) {
                    if (result.pwhash !== null) {
                        bcrypt.compare(password, result.pwhash, function(hashErr, success) {
                            if (success == true) {
                                req.session.name = username;
                                res.json({"status":"success","logged in as":req.session.name});
                            }
                            else {
                                res.json({"status":"error","error":"Invalid parameters"});
                            }
                        })
                    }
                    else {
                        res.json({"status":"error","error":"Target not found in " + col});
                    }    
                }
                else {
                    res.json({"status":"error","error":"Target not found in " + col});
                }    
            });
        })
    }
    else {
        res.json({"status":"error","error":"empty fields"});
    }
});

/**
 * Registration
 */
rest_api.post('/register', function (req, res) {
    if (req.body.email && req.body.reg_username && req.body.reg_password && req.body.passwordConf && !req.session.name) {
        var email = req.body.email;
        var username = req.body.reg_username;
        var password = req.body.reg_password;
        var passwordConf = req.body.passwordConf;
        if (password == passwordConf) {
            bcrypt.hash(password, saltRounds, function(err, hash) {
                if (err) {
                    logger.error(err);
                    res.json({"status":"error","error":"error while hashing"});
                }
                MongoClient.connect(url, function(dbErr, db) {
                    if (dbErr) {
                        logger.error(dbErr);
                        res.json({"status":"error","error":"failed to connect to database"});
                    }

                    var obj = {
                        "userName": username,
                        "pwhash": hash
                    }

                    var collection = db.collection('Users');
                    collection.insertOne({userName:username,pwhash:hash,email:email,timeUserRegistered:new Date(Date.now())}, function(dbErr2, result) {
                        if (dbErr2) {
                            logger.error(dbErr2);
                            res.json({"status":"error","error":"username or email already taken"});
                        }
                        else {
                            logger.info("1 new user iserted to database");
                            res.json({"status":"success"});
                        }
                        db.close();
                    })
                })
            })
        }
        else {
            res.json({"status":"error","error":"passwords differ!"});
        }
    }
    else if (req.session.name) {
        res.json({"status":"error","error":"already logged in!"});
    }
    else {
        res.json({"status":"error","error":"empty fields!"});
    }
});

/**
 * Searches users
 */
rest_api.get('/user/:user', function (req, res) {
    // hakee käyttäjän tiedot
    // tänne varmaan joku sessiopohjainen autentikointi
        
    var user = req.params.user;
    logger.info("Requesting user with name=" + user);

    MongoClient.connect(url, function(err, db) {
        if (err) {
            logger.error(err);
        }

        var collection = db.collection('Users');
        collection.findOne({userName:user},{_id:0,pwhash:0}, function(err, result) {
            if (err) {
                logger.error(err);
            }
            db.close();
            if (result !== null) {
                res.json(result);
            }
            else {
                res.json({"status":"error","error":"Target not found in Users"});
            }            
        });
    });
});

/**
 * Searches users
 * Deprecated, use GET /user/:user instead
 */
rest_api.get('/search/user/:username', function (req, res) {
    // hakee käyttäjän tiedot
    // tänne varmaan joku sessiopohjainen autentikointi
        
    var username = req.params.username;
    logger.info("Requesting user with name=" + username);

    MongoClient.connect(url, function(err, db) {
        if (err) {
            logger.error(err);
        }

        var collection = db.collection('Users');
        collection.findOne({userName:username},{_id:0,pwhash:0}, function(err, result) {
            if (err) {
                logger.error(err);
            }
            db.close();
            if (result !== null) {
                res.json(result);
            }
            else {
                res.json({"status":"error","error":"Target not found in " + collection});
            }            
        });
    });
});

rest_api.patch('/user/:user', function (req, res) {
    var user = req.params.user;
    
    if(!req.session.name) {
        var message = "Can't patch user, not logged in";
        console.log(message);
        logger.info(message);
        res.json({"status":"error","error":message});
    }
    else if (req.session.name !== user) {
        var message = "Can't patch user, wrong user";
        console.log(message);
        logger.info(message);
        res.json({"status":"error","error":message});
    }
    else if (!req.body.password) {
        var message = "Can't patch user, no password supplied";
        console.log(message);
        logger.info(message);
        res.json({"status":"error","error":message});
    }
    else {
        var update = {};
        var changes = [];
        if (req.body.password && req.body.newPassword && req.body.newPassword === req.body.passwordConf) {
            hash = bcrypt.hashSync(req.body.newPassword, saltRounds);
            update.pwhash = hash;
            changes.push("password");
        }
        if (req.body.password && req.body.email) {
            update.email = req.body.email;
            changes.push("email");
        } 
        
        if (req.body.password) {
            MongoClient.connect(url, function (err, db) {
                if (err) {
                    console.log(err);
                    logger.error(err);
                }
                else {
                    db.collection('Users').findOne({userName:req.session.name}, function (err, result) {
                        if (err) {
                            console.log(err);
                            logger.error(err);
                        }
                        else if (result) {
                            if (result.pwhash) {
                                bcrypt.compare(req.body.password, result.pwhash, function(err, success) {
                                    if (err) {
                                        console.log(err);
                                        logger.error(err);
                                        res.json({"status":"error","error":"Error while hashing"});
                                    }
                                    if (success == true) {
                                        db.collection('Users').findOneAndUpdate({userName:user},{$set:update}, function (err, result) {
                                            if (err) {
                                                console.log(err);
                                                console.log("Failed update: ");
                                                console.log(update);
                                                logger.error(err);
                                                res.json({"status":"error","error":"An error occured"});
                                            }
                                            else if (result) {
                                                var message = "Success! Changes: " + changes.toString(); 
                                                console.log(message);
                                                logger.info(message);
                                                res.json({"status":"success","changes":changes});
                                            }
                                            else {
                                                res.json({"status":"error","error":"User not found"});
                                            }
                                        })
                                    }
                                    else {
                                        res.json({"status":"error","error":"Wrong password"});
                                    }
                                })
                            }
                            else {
                                res.json({"status":"error","error":"An error occured"});
                            }
                        }
                    })
                }
            })
        }
        else {
            res.json({"status":"error","error":"No password supplied"});
        }
    }
});

/**
 * Authenticaton
 */
rest_api.get('/status', function(req, res) {
    if (req.session.name) {
        res.json({"logged in":true,"name":req.session.name});
    }
    else {
        res.json({"logged in":false});
    }
});

/**
 * Deletes user
 */
rest_api.delete('/user/:user', function (req, res) {
    if (!req.params.user) {
        console.log("No user supplied for deletion");
        res.status(404).json({"status":"error","error":"No user specified"});
    }
    else if (!req.session.name) {
        console.log("User not logged in for deletion");
        res.status(401).json({"status":"error","error":"Not logged in"});
    }
    else if (req.session.name != req.params.user && req.session.name != null) {
        console.log("Tried to delete a wrong account");
        res.status(403).json({"status":"error","error":"Tried to delete a wrong account"});
    }
    else if (req.session.name == req.params.user && req.session.name != null) {
        var user = req.params.user;
        var message = "Attempting to delete user " + user;
        console.log(message);
        logger.info(message);

        MongoClient.connect(url, function (err, db) {
            if (err) {
                console.log(err);
                logger.error(err);
                res.sendStatus(500);
            }
            else if (db) {
                // [x] check if user exists
                // [x] check if specified user is the same as logged in user (could be admin or something if expanded)
                // [x] user deletion:
                // [x] - remove user
                // [x] - check if user owns sets (and those sets don't have a pending owner)
                // [x]   - if so, sets are deleted and removed from other users (maybe add a confirm)
                // [x] - remove user from sets
                // [x] - remove any PendingOwner entries the user has
                // [x] - remove any requestDevice entries the user has
                // [x] - remove any requestMember entries the user has
                // [x] - remove any user owned devices from sets
                // [x] - de-register all user-owned devices
                db.collection('Users').findOneAndDelete({userName:user}, function (err, result) {
                    if (err) {
                        console.log(err);
                        logger.error(err);
                        res.sendStatus(500);
                    }
                    else if (result.value != null) {
                        db.collection('Sets').find(
                            {owner:user,pendingOwner:{$exists:false}},
                            function(err, result) {
                                if (err) {
                                    console.log(err);
                                    logger.error(err);
                                    res.sendStatus(500);
                                }
                                if (result) {
                                    db.collection('Sets').deleteMany(
                                        {owner:user,pendingOwner:{$exists:false}}
                                    );
                                    result.forEach(function(element) {
                                        db.collection('Users').updateMany(
                                            {sets:element.setName},
                                            {$pull:{sets:elemet.setName}}
                                        )
                                    });
                                }
                            }
                        );
                        db.collection('Sets').updateMany(
                            {owner:user,pendingOwner:{$exists:true}},
                            {$unset:{owner:""}}
                        );
                        db.collection('Sets').updateMany(
                            {members:user},
                            {$pull:{members:user}}
                        );
                        db.collection('Sets').updateMany(
                            {requestMembers:user},
                            {$pull:{requestMembers:user}}
                        );
                        db.collection('Sets').updateMany(
                            {pendingOwner:user},
                            {$unset:{pendingOwner:""}}
                        );

                        db.collection('Devices').find(
                            {owner:user},
                            function (err, result) {
                                if (err) {
                                    console.log(err);
                                    logger.error(err);
                                    res.sendStatus(500);
                                }
                                result.forEach(function(element) {
                                    // remove device from all sets
                                    db.collection('Sets').updateMany(
                                        {devices:element.deviceName},
                                        {$pull:{devices:element.deviceName}}
                                    )
                                    // remove device from requestDevices
                                    db.collection('Sets').updateMany(
                                        {requestDevices:element.deviceName},
                                        {$pull:{requestDevices:element.deviceName}}
                                    )
                                    // unset owner
                                    db.collection('Devices').updateMany(
                                        {owner:user},
                                        {$unset:{owner:""}}
                                    )
                                });
                            }
                        );
                        var message = "User " + user + " deleted";
                        console.log(message);
                        logger.info(message);
                        res.status(200).json({"status":"success","message":message});
                    }
                    else {
                        var message = "User " + user + " not found";
                        console.log(message);
                        logger.info(message);
                        res.status(404).json({"status":"error","error":message});
                    }
                })
            }
            else {
                res.sendStatus(500);
            }
        })
    }
    else {
        res.sendStatus(500);
    }
});
// #endregion Account JS


/* * * * * * * */
/* Set routes  */
/* * * * * * * */

// #region Set JS

/**
 * Searches sets
 */
rest_api.get('/set/:set', function (req, res) {
    // hakee laitteen tiedot
    // tänne varmaan joku sessiopohjainen autentikointi
    
    var set = req.params.set;
    logger.info("Requesting set with name: " + set);
    dbSearch('Sets', 'setName', set, function(result){
        res.json(result);
    });
});

/**
 * Searches sets
 * Deprecated
 */
rest_api.get('/search/set/:id', function (req, res) {
    // hakee laitteen tiedot
    // tänne varmaan joku sessiopohjainen autentikointi
    
    var id = req.params.id;
    logger.info("Requesting set with name: " + id);
    dbSearch('Sets', 'setName', id, function(result){
        res.json(result);
    });
});

/**
 * Join a set
 * Deprecated, use PATCH /set/:set instead
 */
rest_api.post('/set/:set/join', function (req, res) {
    if (!req.session.name) {
        logger.info("Can't join a set, not logged in");
        res.json({"status":"error","error":"not logged in"});
    }
    else {
        var set = req.params.set;
        var userName = req.session.name;
        logger.info("Requesting to join a set with name: " + set);
        MongoClient.connect(url, function(err, db) {
            if (err) {
                logger.error(err);
                res.json({"status":"error","error":"failed to connect to database"});
            }

            var col = db.collection('Sets');

            col.findOne({setName:set}, function(err, result) {
                if (err) {
                    console.log(err);
                    logger.error(err);
                    res.json({"status":"error","error":"set cannot be found"});
                }
                else if (result) {
                    col.findOne({setName:set,$or:[{members:userName},{requestMembers:userName}]}, function(err, result) {
                        if (err) {
                            console.log(err);
                            logger.error(err);
                            res.json({"status":"error","error":"already associated with a set"});
                        }
                        else if (result === null) {
                            col.findOneAndUpdate({setName:set},{$push:{requestMembers:userName}}, function(err, result) {
                                if (err) {
                                    console.log(err);
                                    logger.error(err);
                                    res.json({"status":"error","error":"invalid set name"});
                                }
                                else {
                                    var info = "User " + userName + " added to the join request list of set " + set;
                                    console.log(info);
                                    logger.info("User " + userName + " added to the join request list of set " + set);
                                    res.json({"status":"success"});
                                }
                            })
                        }
                        else {
                            var info = "User " + userName + " is already associated with the set " + set;
                            console.log(info);
                            logger.error(info);
                            res.json({"status":"error","error":"already associated with the set" + set});
                        }
                    })
                }
                else {
                    var info = "Set " + set + " cannot be found";
                    console.log(info);
                    logger.error(info);
                    res.json({"status":"error","error":"set cannot be found"});
                }
            })

        })
    }
});

/**
 * Used for creating sets
 */
rest_api.post('/set/create', function(req, res) {
    if (!req.session.name) {
        logger.info("Can't create a set, not logged in");
        res.json({"status":"error","error":"not logged in"});
    }
    else if (!req.body.setName) {
        logger.info("Empty set name; Can't create a set");
        res.json({"status":"error","error":"empty set name"});
    }
    else {
        MongoClient.connect(url, function(dbErr, db) {
            if (dbErr) {
                logger.error(dbErr);
                res.json({"status":"error","error":"failed to connect to database"});
            }
            db.collection('Sets').insertOne(
                {
                    setName:req.body.setName,
                    owner:req.session.name,
                    timeSetCreated:new Date(Date.now()),
                    members:[req.session.name]
                }, function(err, result) {
                if (err) {
                    logger.error("Set creation: " + err);
                    console.log("Set creation: " + err);
                    res.json({"status":"error","error":"set name taken"});
                }
                else {
                    db.collection('Users').findOneAndUpdate({userName:req.session.name},{$push:{sets:req.body.setName}});
                    logger.info("1 new set inputted to database");
                    res.json({"status":"success"});
                }
            });
        });
    }
});

/**
 * Adds a device to a sets device queue
 */
rest_api.put('/set/:set/:dev', function(req, res) {
    if (req.session.name) {
        var name = req.session.name;
        var set = req.params.set;
        var dev = req.params.dev;
        var message = "Attempting to add device " + dev + " to set " + set;
        console.log(message);
        logger.info(message);
        MongoClient.connect(url, function(err, db) {
            if (err) {
                console.log(err);
                logger.error(err);
                res.json({"status":"error","error":"failed to connect to database"});
            }
            var col = db.collection('Sets');
            col.findOne({setName:set, members:name},{_id:0},function(err, result) {
                if (err) {
                    var message = "user not associated with this set";
                    console.log(message);
                    logger.error(message);
                    res.json({"status":"error","error":message});
                }
                else if (result) {
                    db.collection('Users').findOne({userName:name, devices:dev, sets:set},{_id:0},function(err, result) {
                        if (err) {
                            var message = "something went wrong!";
                            console.log(err);
                            logger.error(err);
                            res.json({"status":"error","error":message});
                        }
                        else if (result) {
                            col.findOneAndUpdate(
                                {setName:set, members:name, $and: [{devices: {$ne: dev}},{requestDevices: {$ne: dev}}]},
                                {$push: {requestDevices: dev}},
                                function(err, result) {
                                    if (err) {
                                        console.log(err);
                                        logger.error(err);
                                        res.json({"status":"error","error":"database error"});
                                    }
                                    else if (result) {
                                        var message = "Device " + dev + " added to queue of " + set;
                                        console.log(message);
                                        logger.info(message);
                                        res.json({"status":"success"});
                                    }
                                    else {
                                        var message = "Device " + dev + " already associated with set " + set;
                                        console.log(message);
                                        logger.info(message);
                                        res.json({"status":"error","error":"device already associated with the set"});
                                    }
                                }
                            )
                        }
                        else {
                            var message = "user " + name + " has no rights to use the device " + dev;
                            console.log(message);
                            logger.info(message);
                            res.json({"status":"error","error":"you have no rights to use this device"});
                        }
                    })
                }
                else {
                    var message = "user not associated with this set";
                    console.log(message);
                    logger.info(message);
                    res.json({"status":"error","error":message});
                }
            })
        })
    }
    else {
        logger.info("Can't add a device to a set, not logged in");
        res.json({"status":"error","error":"not logged in"});
    }
});

/**
 * Used for modifying sets
 */
rest_api.patch('/set/:set', function(req, res) {
    var set;
    var user;
    if (!req.session.name) {
        console.log("Cant modify a set, not logged in");
        logger.info("Cant modify a set, not logged in");
        res.json({"status":"error","error":"not logged in"});
    }
    else if (!req.params.set) {
        console.log("Cant modify a set, no set specified");
        logger.info("Cant modify a set, no set specified");
        res.json({"status":"error","error":"no device specified"});
    }
    else if (req.session.name && req.params.set) {
        set = req.params.set;
        user = req.session.name;
        var message = "User " + user + " is attempting to patch set " + set;

        // check user's set rights
        MongoClient.connect(url, function (err, db) {
            if (err) {
                var message = "PATCH SET: Database connection error. ";
                console.log(message + err);
                logger.info(message + err);
                res.json({"status":"error","error":"database connection error"});
            }
            else if (db) {
                db.collection('Sets').findOne({setName:set},{_id:0}, function (err, result) {
                    if (err) {
                        var message = "PATCH SET: Database connection error. ";
                        console.log(message + err);
                        logger.info(message + err);
                        res.json({"status":"error","error":"database connection error"});
                    }
                    else if (result && result.pendingOwner != null && result.pendingOwner == req.session.name && req.body.owner == req.session.name) {
                        db.collection('Sets').findOneAndUpdate(
                            {setName:set},
                            {$set:{owner:req.session.name},$unset:{pendingOwner:""}},
                            function (err, result) {
                                if (err) {
                                    console.log(err);
                                    logger.error(err);
                                    res.json({"status":"error","error":"database connection error"});
                                }
                                else if (result) {
                                    var message = "User " + user + " is now the owner of the set " + set;
                                    console.log(message);
                                    logger.info(message);
                                    res.json({"status":"success","updates":message});
                                }
                                else {
                                    var message = "Unknown error";
                                    console.log(message);
                                    logger.error(message);
                                    res.json({"status":"error","error":message});
                                }
                            }
                        );
                    }
                    else if (req.body.leave == true && result) {
                        if (result.owner == user) {
                            res.json({"status":"error","error":"owner can't leave the group"});
                        }
                        else {
                            db.collection('Sets').findOneAndUpdate({setName:set,members:user},
                                {$pull:{members:user}},
                                function(err, result) {
                                    if (err) {
                                        console.log(err);
                                        logger.error(err);
                                        res.json({"status":"error","error":"database connection error"});
                                    }
                                    else if (result) {
                                        var message = "User " + user + " left set " + set;
                                        db.collection('Users').findOneAndUpdate({userName:user},{$pull:{sets:set}});
                                        console.log(message);
                                        logger.info(message);
                                        res.json({"status":"success","message":message});
                                    }
                                    else {
                                        db.collection('Sets').findOneAndUpdate(
                                            {setName:set,requestMembers:user},
                                            {$pull:{requestMembers:user}}, 
                                            function(err, result) {
                                                if (err) {
                                                    console.log(err);
                                                    logger.error(err);
                                                    res.json({"status":"error","error":"database connection error"});
                                                }
                                                else if (result) {
                                                    var message = "User " + user + " left queue of set " + set;
                                                    db.collection('Users').findOneAndUpdate({userName:user},{$pull:{sets:set}});
                                                    console.log(message);
                                                    logger.info(message);
                                                    res.json({"status":"success","message":message});
                                                }
                                                else {
                                                    res.json({"status":"error","error":"set not found"});
                                                }
                                            }
                                        );
                                    }
                                }
                            );
                        }
                    }
                    else if (req.body.pullDevice != null && result) {
                        var pullDevice = req.body.pullDevice;
                        db.collection('Devices').findOne({deviceName:pullDevice,owner:user}, function (err, result) {
                            if (err) {
                                console.log(err);
                                logger.error(err);
                                res.json({"status":"error","error":"database connection error"});
                            }
                            else if (result) {
                                db.collection('Sets').findOneAndUpdate(
                                    {setName:set,devices:pullDevice},
                                    {$pull:{devices:pullDevice}},
                                    function (err, result) {
                                        if (err) {
                                            console.log(err);
                                            logger.error(err);
                                            res.json({"status":"error","error":"database connection error"});
                                        }
                                        else if (result) {
                                            var message = "User " + user + " removed device " + pullDevice + " from set " + set;
                                            console.log(message);
                                            logger.info(message);
                                            res.json({"status":"success","message":message});
                                        }
                                    }
                                );
                            }
                        });
                    }
                    else if (result) {
                        var isOwner = false;
                        if (result.owner == user) {
                            isOwner = true;
                        }

                        var update = {};
                        if (!isOwner && req.body.newPendingMember)  update.requestMembers   = req.body.newPendingMember;
                        if (!isOwner && req.body.newPendingDevice)  update.requestDevices   =  req.body.newPendingDevice;
                        if (isOwner && req.body.acceptedDevice)     update.devices          =  req.body.acceptedDevice;
                        if (isOwner && req.body.acceptedUser)       update.members          =  req.body.acceptedUser;
                        if (isOwner && req.body.bannedUser)         update.bannedUsers      =  req.body.bannedUser;
                        if (isOwner && req.body.unbannedUser)       update.members          =  req.body.unbannedUser;

                        var pendingOwner        = (isOwner && req.body.newPendingOwner)     ? req.body.newPendingOwner  : null;

                        
                        if (pendingOwner) {
                            db.collection('Sets').findOneAndUpdate(
                                {setName:set, members:user},
                                {$set:{pendingOwner:pendingOwner}}
                            );
                        }
                        if (Object.keys(update).length !== 0) {
                            db.collection('Sets').findOneAndUpdate(
                            {setName:set},
                            {$push:update}, 
                            function (err, result) {
                                if (err) {
                                    var message = "PATCH SET: Database connection error. ";
                                    console.log(message + err);
                                    logger.info(message + err);
                                    res.json({"status":"error","error":"database connection error"});
                                }
                                else if (result) {
                                    if (req.body.acceptedUser) {
                                        db.collection('Sets').findOneAndUpdate(
                                            {setName:set},
                                            {$pull:{requestMembers:req.body.acceptedUser}}
                                        );
                                        db.collection('Users').findOneAndUpdate(
                                            {userName:req.body.acceptedUser},
                                            {$push:{sets:set}}
                                        )
                                    }
                                    if (req.body.acceptedDevice) {
                                        db.collection('Sets').findOneAndUpdate(
                                            {setName:set},
                                            {$pull:{requestDevices:req.body.acceptedDevice}}
                                        );
                                    }
                                    var message = "User " + user + " changed set " + set + "; " + update.toString();
                                    console.log(message);
                                    logger.info(message);
                                    res.json({"status":"success","updates":update});
                                }
                                else {
                                    var message = "User " + user + " does not have rights to manipulate set " + set;
                                    console.log(message);
                                    logger.info(message);
                                    res.json({"status":"error","error":"you do not have the rights for this set"});
                                }
                            });
                        }
                        else {
                            var message = "No data received!";
                            console.log(message);
                            logger.info(message);
                            res.json({"status":"error","error":message});
                        }
                    }
                    else {
                        var message = "User " + user + " does not have rights to manipulate set " + set;
                        console.log(message);
                        logger.info(message);
                        res.json({"status":"error","error":"you do not have the rights for this set"});
                    }
                });
            }
            else {
                res.json({"status":"error","error":"An error occured"});
            }
        });
    }
    else {
        res.json({"status":"error","error":"Bad parameters"});
    }
});

rest_api.delete('/set/:set', function(req, res) {
    var set;
    var user;

    if (!req.session.name) {
        var message = "not logged in";
        console.log(message);
        logger.error(message);
        res.json({"status":"error","error":message});
    }
    else if (req.session.name && req.body.confirm == "true" && req.params.set) {
        console.log(req.session.name + ";" + req.body.confirm + ";" + req.params.set);
        user = req.session.name;
        set = req.params.set;
        MongoClient.connect(url, function(err, db) {
            if (err) {
                console.log(err);
                logger.error(err);
                res.json({"status":"error","error":"database connection error"});
            }
            else if (db) {
                db.collection('Sets').findOneAndDelete({setName:set,owner:user},function(err, result) {
                    if (err) {
                        console.log(err);
                        logger.error(err);
                        res.json({"status":"error","error":"database connection error"});
                    }
                    else if (result.value != null) {
                        console.log(result);
                        db.collection('Users').updateMany({sets:set},{$pull:{sets:set}});
                        var message = "User " + user + " has deleted set " + set;
                        console.log(message);
                        logger.info(message);
                        res.json({"status":"success","updates":message});
                    }
                    else {
                        var message = "no set found";
                        console.log(message);
                        logger.error(message);
                        res.json({"status":"error","error":message});
                    }
                })
            }
            else {
                var message = "unknown error";
                console.log(message);
                logger.error(message);
                res.json({"status":"error","error":message});
            }
        })
    }
    else {
        var message = "invalid parameters";
        console.log(message);
        logger.error(message);
        res.json({"status":"error","error":message});
    }
});
// #endregion Set JS


/* * * * * * * * */
/* Device routes */
/* * * * * * * * */

// #region Device JS

/*
 * Searches devices
 * Deprecated, use GET /device/:device instead
 */
rest_api.get('/search/:id', function (req, res) {
    var id = req.params.id;
    logger.info("Requesting device with name: " + id);
    dbSearch('Devices', 'deviceName', id, function(result){
        res.json(result);
    });
});

/**
 * Searches device values
 * Deprecated, use GET /device/:device/values instead
 */
rest_api.get('/search/values/:id', function (req, res) {
    // hakee laitteen tiedot
    // tänne varmaan joku sessiopohjainen autentikointi
    if (req.params.id) {
        var id = req.params.id;
        logger.info("Requesting values for device with name=" + id);

        var values_json = [];
        var query = {};
        query["device"] = id;
        if (req.query.after && req.query.before) {
            var after = new Date(req.query.after);
            var before = new Date(req.query.before);
            query["time"] = {$gte: after, $lte: before};
        }
        else if (req.query.after) {
            var after = new Date(req.query.after);
            query["time"] = {$gte: after};
        }
        else if (req.query.before) {
            var before = new Date(req.query.before);
            query["time"] = {$lte: before};
        }

        MongoClient.connect(url, function(err, db) {
            if (err) {
                console.log(err);
                logger.error(err);
            }

            var collection = db.collection('Values');

            var stream;
            if (req.query.count) {
                stream = collection.find(query,{_id:0}).sort({_id:-1}).limit(parseInt(req.query.count)).stream();
            }

            else {
                stream = collection.find(query,{_id:0}).stream();
            }

            stream.on("data", function(value) {
                values_json.push(value);
            });
            stream.on("end", function() {
                db.close();
                res.json(values_json.sort(function(a,b) {
                    return a.time - b.time;
                }));
            });
        });
    }
    else {
        res.json({"status":"error","error":"no device specified"})
    }
});

/**
 * Searches device values
 * Possible GET parameters: `before` <timestamp>, `after` <timestamp>, `newest` <count>
 */
rest_api.get('/device/:device/values', function (req, res) {
    // hakee laitteen tiedot
    // tänne varmaan joku sessiopohjainen autentikointi
    if (req.params.device) {
        var device = req.params.device;
        logger.info("Requesting values for device with name=" + device);

        var values_json = [];
        var query = {};
        query["device"] = device;
        if (req.query.after && req.query.before) {
            var after = new Date(req.query.after);
            var before = new Date(req.query.before);
            query["time"] = {$gte: after, $lte: before};
        }
        else if (req.query.after) {
            var after = new Date(req.query.after);
            query["time"] = {$gte: after};
        }
        else if (req.query.before) {
            var before = new Date(req.query.before);
            query["time"] = {$lte: before};
        }

        MongoClient.connect(url, function(err, db) {
            if (err) {
                console.log(err);
                logger.error(err);
            }

            var collection = db.collection('Values');

            var stream;
            if (req.query.count) {
                stream = collection.find(query,{_id:0}).sort({_id:-1}).limit(parseInt(req.query.count)).stream();
            }

            else {
                stream = collection.find(query,{_id:0}).stream();
            }

            stream.on("data", function(value) {
                values_json.push(value);
            });
            stream.on("end", function() {
                db.close();
                if (values_json.length > 0) {
                    res.json(values_json.sort(function(a,b) {
                        return a.time - b.time;
                    }));
                }
                else {
                    res.json({"status":"error","error":"no values found"});
                }
            });
        });
    }
    else {
        res.json({"status":"error","error":"no device specified"});
    }
});

/*
 * Searches devices
 */
rest_api.get('/device/:device', function (req, res) {
    var device = req.params.device;
    logger.info("Requesting device with name: " + device);
    dbSearch('Devices', 'deviceName', device, function(result){
        res.json(result);
    });
});

/**
 * Used to register an unregistered device to an user
 * Deprecated, use PATCH /device/:device instead
 */
rest_api.get('/device/register/:device', function(req, res) {

    logger.info("Attempting to register a device...");

    if (req.query.devicename && req.session.name) {
        var devicename = req.query.devicename;

        MongoClient.connect(url, function(dbErr, db) {
            if (dbErr) {
                logger.error("DB connection error:" + dbErr);
            }
            
            var col1 = 'Devices';

            // Check if device exists in DB AND doesn't have a user associated with it
            // If true, add user to device as an owner
            db.collection('Devices').findOne({deviceName:devicename, owner:{$exists: false}},{_id:0}, function(dbErr2, device) {
                if (dbErr2) {
                    logger.error(dbErr2);   
                }
                if (device) {
                    db.collection('Devices').findOneAndUpdate({deviceName:devicename},{$set:{owner:req.session.name},$currentDate:{timeDeviceRegistered:true}});
                    db.collection('Users').findOneAndUpdate({userName:req.session.name},{$push:{devices:devicename}});
                    logger.info("Device " + devicename + " registered to user " + req.session.name);
                    res.json({"status":"success"});
                    db.close();
                }
                else {
                    logger.info("Device registration failed");
                    res.json({"status":"error","error":"device registration failed"});
                    db.close();
                }
            })
        })
    }
    else if (!req.session.name) {
        logger.info("Can't register a device, not logged in");
        res.json({"status":"error","error":"not logged in"});
    }
    else if (!req.query.devicename) {
        logger.info("Can't register a device, no device specified");
        res.json({"status":"error","error":"no device specified"});
    }
});

rest_api.patch('/device/:device', function(req, res) {
    if (!req.session.name) {
        console.log("Can't patch a device, not logged in");
        logger.info("Can't patch a device, not logged in");
        res.json({"status":"error","error":"not logged in"});
    }
    else if (!req.params.device) {
        console.log("Can't patch devices, no device specified");
        logger.info("Can't patch devices, no device specified");
        res.json({"status":"error","error":"no device specified"});
    }
    else if (!req.body.newPendingOwner && !req.body.newFrequency && !req.body.owner) {
        console.log("Can't patch devices, no parameters");
        logger.info("Can't patch devices, no parameters");
        res.json({"status":"error","error":"no parameters"});
    }
    else {
        var device  = req.params.device ? req.params.device : null;
        var newOwner = (req.body.owner == req.session.name) ? req.session.name : null;
        MongoClient.connect(url, function(err, db) {
            if (err) {
                console.log(err);
                logger.error(err);
                res.json({"status":"error","error":"database error"});
            }
            var update = {};
            if (req.body.newPendingOwner) {
                update.pendingOwner = req.body.newPendingOwner;
            }
            if (req.body.newFrequency) {
                update.frequency = req.body.newFrequency;
            }
            //update.pendingOwner = req.body.newPendingOwner  ? req.body.newPendingOwner  : null;
            //update.frequency    = req.body.newFrequency     ? req.body.newFrequency     : null;
            console.log(update);

            db.collection('Devices').findOne({deviceName:device}, function(err, result) {
                if (err) {
                    console.log(err);
                    logger.error(err);
                    res.json({"status":"error","error":"database error"});
                }
                else if (result) {
                    if (result.owner != null && result.owner == req.session.name) {
                        db.collection('Devices').findOneAndUpdate({deviceName:device,owner:req.session.name},{$set:update}, function(err, result) {
                            if (err) {
                                console.log(err);
                                logger.error(err);
                                res.json({"status":"error","error":"database error"});
                            }
                            else if (result) {
                                console.log("Device modified");
                                logger.info("Device modified");
                                res.json({"status":"success"});
                            }
                            else {
                                var message = "Device " + device + " not found";
                                console.log(message);
                                logger.info(message);
                                res.json({"status":"error","error":message});
                            }
                        });
                    }
                    else if ((result.owner == null && newOwner != null) || (result.pendingOwner != null && result.pendingOwner == newOwner)) {
                        console.log(result.owner + ";" + newOwner + ";" + result.pendingOwner);
                        var old_owner = result.owner;
                        update.owner = newOwner;
                        // $or owner:old_owner
                        // Owner ei päivity oikein!
                        db.collection('Devices').findOneAndUpdate({deviceName:device,$or:[{owner:{$exists:false}},{owner:old_owner}]},{$set:update, $unset:{pendingOwner:""}}, function(err, result) {
                            if (err) {
                                console.log(err);
                                logger.error(err);
                                res.json({"status":"error","error":"database error"});
                            }
                            else if (result) {
                                db.collection('Users').findOneAndUpdate({userName:old_owner},{$pull:{devices:device}});
                                db.collection('Users').findOneAndUpdate({userName:newOwner},{$addToSet:{devices:device}}, function(err, result) {
                                    if (err) {
                                        console.log(err);
                                        logger.error(err);
                                        res.json({"status":"error","error":"database error"});
                                    }
                                    else if (result) {
                                        console.log("Device modified; New device owner");
                                        logger.info("Device modified; New device owner");
                                        res.json({"status":"success", "message":"owner changed"});
                                    }
                                    else {
                                        var message = "User " + req.session.name + " not found";
                                        console.log(message);
                                        logger.info(message);
                                        res.json({"status":"error","error":message});
                                    }
                                });
                            }
                            else {
                                var message = "Device " + device + " not found without owner";
                                console.log(message);
                                logger.error(message);
                                res.json({"status":"error","error":message});
                            }
                        });
                    }
                    else {
                        var message = "Invalid parameters";
                        console.log(message);
                        logger.info(message);
                        res.json({"status":"error","error":message});
                    }
                }
                else {
                    var message = "Device " + device + " not found";
                    console.log(message);
                    logger.info(message);
                    res.json({"status":"error","error":message});
                }
            })
        })
    }
});

// #endregion Device JS

/**
 * Start the rest_api
 */
//rest_api.listen(port);
module.exports = rest_api;
