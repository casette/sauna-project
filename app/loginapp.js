var express = require('express');
var loginapp = express();
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var session = require('express-session');
var MongoStore = require('connect-mongo')(session);

//connect to MongoDB
mongoose.connect('mongodb://localhost:27017/SaunaDB');
var db = mongoose.connection;

//handle mongo error
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
  // we're connected!
});

//use sessions for tracking logins
loginapp.use(session({
  secret: 'work hard',
  resave: true,
  saveUninitialized: false,
  store: new MongoStore({
    mongooseConnection: db
  })
}));

// parse incoming requests
loginapp.use(bodyParser.json());
loginapp.use(bodyParser.urlencoded({ extended: false }));

// PÄIVITÄ TÄMÄ HTML SIVULLE
// serve static files from template
loginapp.use(express.static(__dirname + '/templateLogReg'));

// include routes
var routes = require('./routes/router');
loginapp.use('/', routes);

// catch 404 and forward to error handler
loginapp.use(function (req, res, next) {
  var err = new Error('File Not Found');
  err.status = 404;
  next(err);
});

// error handler
// define as the last app.use callback
loginapp.use(function (err, req, res, next) {
  res.status(err.status || 500);
  res.send(err.message);
});


// listen on port 3000
loginapp.listen(3000, function () {
  console.log('Express app listening on port 3000');
});
