var express     = require('express');
var app         = express();
var session     = require('express-session');
var rest_api    = require("./rest_api");
var register    = require("./add-device");
var dbConfig    = require('./config/database.js');
var bodyParser  = require('body-parser');
var url         = dbConfig.url;
var port        = 3000;
var mongoSanitize = require('express-mongo-sanitize');

const MongoStore = require('connect-mongo')(session);

const { createLogger, format, transports }  = require('winston');
const { combine, timestamp, label, printf } = format;

const myFormat = printf(info => {
    return `${info.timestamp} [${info.label}] ${info.level}: ${info.message}`;
  });
const logger = createLogger({
  level: 'info',
  format: format.combine(
      label({label:'Rest API'}),
      timestamp(),
      myFormat
    ),
  transports: [
    //
    // - Write to all logs with level `info` and below to `combined.log` 
    // - Write all logs error (and below) to `error.log`.
    //
    new transports.File({ filename: 'logs/error.log', level: 'error', 'timestamp':true }),
    new transports.File({ filename: 'logs/combined.log', 'timestamp':true })
  ]
});

app.use(session({
    secret: 'falafel',
    resave: false,
    unset: 'destroy',
    saveUninitialized: false,
    store: new MongoStore({ url: url })
}))

app.use('/api', rest_api);
app.use('/dev', register);


app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(mongoSanitize());

app.use(express.static('html'));
app.get("/", function(req, res) {
    if (req.session.name) {
        res.sendFile(__dirname + "/html/" + "dash.html")
    }
    else {
        res.sendFile(__dirname + "/html/" + "landing.html");
    }
})

app.listen(port);
