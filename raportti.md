# 1.	JOHDANTO	

Projektiryhmässä tehtiin web-applikaatio ja backend järjestelmälle joka hakee lämpötilatietoja mittarista ja näyttää ne verkkosivulla. Työ tehtiin tilauksena Etteplanilta osana Ohjelmistoprojekti-kurssia JAMK:issa. Työ tehtiin keväällä 2018, mutta dokumentaatio alkoi jo syksyllä 2017. Projektiin osallistuivat Juuso Minkkilä, Toni Pajukanta, Timo Heinonen, Mikko Taipale ja Jussi Hurtta. Syksyllä projektiryhmässä oli mukana myös Borhan Amini, mutta hän poistui projektista. Tämä dokumentti toimii projektin loppuraporttina.

# 2.	TEHTÄVÄ, TAVOITE, TULOKSET

## 2.1.	Yhteenveto projektin toteumasta
Koko projektin toteuma (projektisuunnitelman tavoitteisiin ja tehtäviin peilaten): 
* Mitä piti tehdä 
  * Fullstack toteutus
  * Anturilaite lähettää dataa TTNään
  * Data haetaan näytettäväksi
* Mitä tehtiin 
  * Arduino lähetää LoRaWANilla dataa TTNään (ABP)
  * Data haetaan TTNstä kantaan
  * Data haetaan kannasta REST-rajapinnan kautta
  * Data näytetään nettisivulla
  * Palvelimella HTTPS ja reverse proxy Nginx:llä
  * RobotFramework testejä
  * Systeemistä tehdään kontti
  * Käyttäjäsysteemi
  * Ryhmittelysysteemi
* Miten onnistuttiin
  * Perusominaisuudet toimivat haluamallamme tavalla
  * Järjestelmä on funktionaalinen mutta ehkä hiukan hiomaton
  * Tuottamamme systeemi on tekninen alfa

Tarkoituksenamme oli tuottaa fullstack-ratkaisu IoT-lämpömittareille. Yksinkertaisuudessaan anturilaite lähetää LoRaWAN-teknologialla lämpötiladatansa The Things Network middlewarelle, josta toteuttamamme NodeJS back-end noutaa sen tietokantaan. Tietokantaan asetettu data näytetään verkkosivulla. 

## 2.2.	 Projektin onnistuminen (suunnitelma vs. toteutuma)
* Prosessin rakenne
  * Syksyllä suunniteltiin ja tutkittiin, keväällä toteutettiin
  * Sprinttien sijaan käytettiin Kanban-mentelmää
  * Syksyllä kaikki tekivät suunnittelua ja tutkimusta, keväällä tehtävät olivat:
    * Toni Pajukanta: Sulautettu devaaja
    * Jussi Hurtta: Kanta-/palvelinosaaja
    * Juuso Minkkilä: NodeJS backend devaaja / testaaja / projektipäällikkö
    * Mikko Taipale: Front-end devaaja
    * Timo Heinonen: CI-/Docker-osaaja
  * Aikatauluja ei varsinaisesti ollut, teimme täydellä pöhinällä kaiken, minkä ehdimme
* Projektin sisäiset aikaresurssit
  * Kevään aikana projektiin käytettiin aikaa noin kolme kokonaista työpäivää viikossa per henkilö
  * Projektin hallintaan kului ehkä noin 15% ajasta
* Projektin ulkopuoliset resurssit
  * Ulkopuoliset resurssit rajoittuvat toimeksiantajan ja projektiryhmän välisiin tapaamisiin, jotka toimivat koulutuksena ja konsultaationa
  * Lisäksi jouduimme ottamaan yhteyttä JAMKin Labranetin IT-tukeen


# 3.	ONGELMAT JA NIIDEN RATKAISUT
Tässä kappaleessa kuvataan projektin aikana ilmenneet ongelmat ja niiden rarkaisut.

## 3.1.	Ongelmat suunnittelussa
* Jotkin ominaisuudet olisi voinut speksata paremmin
* Osa kosepteista oli erilaisia kuin oltiin suunnittelun aikana odotettu
* Monta kertaa kävi niin, että suunnitellut mockit eivät vastanneet toteutusta ollenkaan
* Muutaman kerran kävi myös niin, että suunnitteludokumentit olivat jäljessä toteutusta

## 3.2.	Ongelmat toteutuksessa
* Arduinon downlink ei toiminut niinkuin pitäisi, syyksi epäillää laitevikaa
* Tietokannan scheemasta kävi ilmi, että se hajoiaisi ennen pitkää
  * Tämä korjattiin scheemaa muuttamalla
* CI-ketjun suorittaessa testejä pääpalvelimen portit menevät väliaikaisesti tukkoon
* GitLab-runner söi paljon resursseja palvelimelta
  * Korjattiin optimoimalla CI-tiedostoa sekä muutamalla palvelimen uudelleenkäynnistyksellä
* Virallisesti allekirrjoitettu SSL sertifikaatti olisi vaatinut domainin
  * Kierrettiin allekirjoittamalla oma sertifikaatti. Tämä aiheuttaa varoituksen selaimella, mutta on riittävän hyvä tähän projektiin
* MongoDB-kannan suojauksen tila on epäselvä
  * Ei olla varmoja, eikö osata injektoida, vai onko injektiot estetty oikein
* Front-endissä käytetty grafiikkakirjasto vuotaa muistia
  * Huomattiin liian myöhään, että asialle voisi mitään tehdä
* Laitteen dokumentaatio oli osittain vajaavainen
  * Ongelmat saatiin ratkaistua syväluotaavalla tukinnalla

## 3.3.	Muut ongelmat tai toteutuneet riskit ja niiden käsittely
* Docker-kontti ei toimi ihan niin kuin pitäisi
* Toimeksiantajan kanssa tapahtuneet kommunikaatioesteet aiheuttivat sen, ettei downlink-ongelmia saatu korjattua ajoissa
* Työtilan ilmanvaihto oli huono


# 4.	YHTEENVETO
## 4.1.	Keskeiset opit

* Speksauksen oleellisuus verestyi
* Työtä löytyy aina lisää jos sitä etsii, eikä aikaa ole koskaan riittävästi
* Ei kannata laittaa palomuuria päälle ilman mitään parametreja
* Docker ei ollutkaan "ihan hello ja yksinkertainen"
* MongoDBssä ei ole transaktioita tai sub-queryitä ollenkaan (vielä)
* Kaikki oppivat vähän jotakin kaikista projektin alueista, myös oman alueensa ulkopuolelta
* Projektityöskentelyä

## 4.2.	Itsearviointi

### 4.2.1.	Ryhmätyö

* Kaikki ryhmän jäsenet olivat paikalla työpäivinä lukuun ottamatta sairaustapauksia
* Ryhmän jäsenten eri vahvuuksia hyödynnettiin parhaan mukaan työnjaossa
* Ongelmat ratkaistiin pitkälti tietoa etsimällä ja eri lähestymistapoja punnitsemalla
* Työnjako ja tehtävien hallinta onnitui varsin hyvin
* Ryhmätyö oli sujuvaa, ja ryhmä tiivityi projektin edetessä
* Ensisijaisija resurssejamme olivat käytössämme olevat tilat, oma aikamme, ja vappaasti saatavilla oleva tieto
* Marko Rintamäen lisäksi ohjausta saatiin myös Juho Pekiltä
* Projektin aikana kriiseiltä onneksi vältyttiin, lukuunottamatta erään projektiryhmän jäsenen ryhmästä poistumista

### 4.2.2.	Suunnitelmallisuus (projektityöskentely) 

* Toteutus ei täysin vastannut suunnitelmia, mutta täyttää kuitenkin niiden asettamat vaatimukset.
* Projektin aikana työaikoja valvottiin Toggl:lla
* Projektityöskentely tehtiin Kanban-metodilla niin, että varsinaisia sprinttejä ei ollut, vaan tehtävät tehtiin issue trackerin mukaan.
* Ryhmämme ainut hallittavissa oleva resurssi oli aika. Jokainen ryhmän jäsen sai vastuualueensa kuntoon siihen vaaditussa ajassa.
* Suurin osa tapaamisista otettiin nauhalle ja ladattiin projektiryhmän sisäiselle Slack-kanavalle
* Suunnitelmat, ja varsinkin speksit, tehtiin parhaan mukaan syksyn aikana, ja tarvittamia lisäsuunnitelmia tehtiin kevään aikana lisää. Tarpeen mukaan dokumentteja päivitettiin, jos jotakin suunnitelmaa oli tarve laajentaa.

### 4.2.3.	Vuorovaikutus

* Yhteydenpito sidosryhmiin suoritettiin kommunikaatiosuunnitelman mukaisesti. Ensisijaisesti kommunikointiin siis käytettiin Slackia ja sähköpostia.
* Kommunikointiongelmia oli projektityhmän ja toimeksiantajan välillä muutamia, mutta nämä eivät kriittisesti haitanneet projektin etenemistä. 
* Projektiin liittyvät tapaamiset saatiin onnituneesti sovittua sähköpostitse tai Slackissa, ja niiden läpivienti oli myös sujuvaa.
* Ryhmän sisäinen kommunikointi tapahtui varsin sujuvasti, sillä kaikki ryhmän jäsenet toimivat joka päivä yhteisessä tilassa.
* Vuorovaikutuksesta jäi kaikille osapuolille varsin hyvä maku suuhun.

### 4.2.4.	Asenne

* Kaikki ryhmän jäsenet olivat aktiivisesti paikalla kaikkina työpävinä sairaustapauksia lukuunottamatta.
* Kaikki ryhmän jäsenet olivat kiinnostuneita projektin aiheesta.
* Kaikki ryhmän jäsenet suorittivat oman osuutensa projektista kunnialla.
* Suurin osa onglemista saatiin ryhmänä ratkaistua.
* Projektista saatu palaute oli pitkälti poisitiivista.

### 4.2.5.	Tulos

* Projektin tulos on vaatimusten ja odotusten mukainen IoT-lämpömittarisysteemi.
* Projektin aikana oppi tulemaan toimeen erilaisten persoonien kanssa ja välttämään turhaa konfliktia.
* Tulos on käytettävissä tarkoituksenmukaisesti referenssinä muille sidosryhmille.
* Projektin tulos palautetaan sellaisenaan. Jatkotyölle ei ole tämänhetkisiä suunnitelmia, mutta tulos on tarvittaessa muokattavissa ja laajennettavissa.

## 4.3.	Arvosanaehdotukset

Projektiryhmän raadin äänestyksen perusteella arvosanaehdotukset jakautuvat seuraavasti:

| Nimi           | Opiskelijatunnus | Arvosanaehdotus |
|:--------------:|:----------------:|:---------------:|
| Juuso Minkkilä | K1756            | 4               |
| Toni Pajukanta | H9281            | 2               |
| Timo Heinonen  | H8262            | 3               |
| Mikko Taipale  | K1732            | 3               |
| Jussi Hurtta   | H9111            | 3               |