#include <SPI.h>
#include <hal/hal.h>
#include <lmic.h>
#include <OneWire.h>

//Extras
#include <LowPower.h> 
#include <configKey.h>



// These callbacks are only used in over-the-air activation, so they are
// left empty here (we cannot leave them out completely unless
// DISABLE_JOIN is set in config.h, otherwise the linker will complain).

void os_getArtEui(u1_t *buf) {}
void os_getDevEui(u1_t *buf) {}
void os_getDevKey(u1_t *buf) {}



static osjob_t sendjob;

//Global values used to get multiple sets of data from function without
// needing to use pointers. Avoid using them, outside of managing messages
// and keep in mind that they are globals
int msg1;
int msg2;
int msg3;
int msg4;
int rawHeat;

void onEvent(ev_t ev) {
  Serial.print(os_getTime());
  Serial.print(": ");
  switch (ev) {
  case EV_SCAN_TIMEOUT:
    Serial.println(F("EV_SCAN_TIMEOUT"));
    break;
  case EV_BEACON_FOUND:
    Serial.println(F("EV_BEACON_FOUND"));
    break;
  case EV_BEACON_MISSED:
    Serial.println(F("EV_BEACON_MISSED"));
    break;
  case EV_BEACON_TRACKED:
    Serial.println(F("EV_BEACON_TRACKED"));
    break;
  case EV_JOINING:
    Serial.println(F("EV_JOINING"));
    break;
  case EV_JOINED:
    Serial.println(F("EV_JOINED"));
    //if OTAA
    //LMIC_setLinkCheckMode(0);
    break;
  case EV_RFU1:
    Serial.println(F("EV_RFU1"));
    break;
  case EV_JOIN_FAILED:
    Serial.println(F("EV_JOIN_FAILED"));
    break;
  case EV_REJOIN_FAILED:
    Serial.println(F("EV_REJOIN_FAILED"));
    break;
  case EV_TXCOMPLETE:
    Serial.println(F("EV_TXCOMPLETE (includes waiting for RX windows)"));
    if (LMIC.txrxFlags & TXRX_ACK)
      Serial.println(F("Received ack"));
    if (LMIC.dataLen) { 
      //downlink doesn't seem to work on this device
      //as in it worked twice and never worked again
      Serial.println(F("Received "));
      Serial.println(LMIC.dataLen);
      Serial.println(F(" bytes of payload"));

    }
    // Schedule next transmission
    os_setTimedCallback(&sendjob, os_getTime() + sec2osticks(TX_INTERVAL),
                        do_send);
    TXLED0; // turn tx LED on
    delay(1000);
    break;
  case EV_LOST_TSYNC:
    Serial.println(F("EV_LOST_TSYNC"));
    break;
  case EV_RESET:
    Serial.println(F("EV_RESET"));
    break;
  case EV_RXCOMPLETE:
    // data received in ping slot
    Serial.println(F("EV_RXCOMPLETE"));
    break;
  case EV_LINK_DEAD:
    Serial.println(F("EV_LINK_DEAD"));
    break;
  case EV_LINK_ALIVE:
    Serial.println(F("EV_LINK_ALIVE"));
    break;
  default:
    Serial.println(F("Unknown event"));
    break;
  }
}

void do_send(osjob_t *j) {
  // Check if there is not a current TX/RX job running
  if (LMIC.opmode & OP_TXRXPEND) {
    Serial.println(F("OP_TXRXPEND, not sending"));
  } else {
    // Prepare upstream data transmission at the next possible time.




    MessageData(messageType); //inserts data into the message according to messageType



    //Restrict when the machine sends messages, by putting it in sleep mode unless messageType based requirements are filled
    if (wannaSleep(messageType)) {
    
    //defining message
    uint8_t msgData[6] {messageType, msg1, msg2 ,msg3, msg4};
    
    //resets the global msg and rawHeat values to be used again
    emptyMessage();

    LMIC_setTxData2(1, msgData, sizeof(msgData) - 1, 0);
    //sending the data will activate an event
    
    

    } else {
      

      //not sending the data will cause you to order the next job manually, aka not through an event
      os_setTimedCallback(&sendjob, os_getTime() + sec2osticks(1),
                        do_send);

    delay(2000);  //to avoid sleep mode from cutting the sleep mode message

      // Sleep mode is for preserving energy,
      // but also has to be noted to cause problem with the USB connection,
      // and stops debug messages from going through.
      
      LowPower.powerDown(sleepTimer, ADC_OFF, BOD_OFF);

     
    }                          
    
   
    
    
  }
  // Next TX is scheduled after TX_COMPLETE event.
}


//clears the globals
void emptyMessage() {

    msg1 = 0; 
    msg2 = 0;
    msg3 = 0; 
    msg4 = 0;  
    rawHeat = 0;

}

//takes the data gained by Arduino andd organizes it for message based on the messageType
void MessageData (int MessageType) {
  
  //sensor tempature
  int tempRead = getTempSensor();
  int Tc_100 = (6 * tempRead) + tempRead / 4;

  //machine tempature
  int temp = getTempComp();

   switch(MessageType) {
      case 0x01 :
           
       
        msg1 = 0; 
        msg2 = 0;
        msg3 = Tc_100 / 100;  
        msg4 = Tc_100 % 100;  
        rawHeat = Tc_100;
         break;
      case 0x02 :
         
         break;
      case 0x03 :
         
         break;
      case 0x04 :
         
         break;
      case 0x11 :
        
        msg3 = highByte(temp);
        msg4 = lowByte(temp);

        
        
        msg1 = Tc_100 / 100;  
        msg2 = Tc_100 % 100;
        rawHeat = Tc_100;
         break;
      default :
         Serial.println(F("Error, no message type"));
   }
   
   
 
   return;
}


//Function has different ifs for different messagetypes
boolean wannaSleep (int MessageType) {

   switch(MessageType) {
      case 0x01 :
          if (rawHeat > tresshold2*100) {
            Serial.println(rawHeat);
            Serial.println(tresshold2*100);
            Serial.println(F("Tempature check, over the limit, sending data")); 
              return true;
          } else {
            Serial.println(rawHeat);
            Serial.println(tresshold2*100);
            Serial.println(F("Tempature check, under limit, data send denied, going to sleep"));
            return false;
          }
       
      
        
         break;
      case 0x02 :
         
         break;
      case 0x03 :
         
         break;
      case 0x04 :
         
         break;
      case 0x11 :
        if (rawHeat > tresshold2*100) {
            Serial.println(rawHeat);
            Serial.println(tresshold2*100);
            Serial.println(F("Tempature check, over the limit, sending data")); 
              return true;
          } else {
            Serial.println(rawHeat);
            Serial.println(tresshold2*100);
            Serial.println(F("Tempature check, under limit, data send denied, going to sleep"));
            return false;
          }
        
         break;
      default :
         Serial.println(F("Error, no message type"));
   }
   
   
 
   
}

int getTempSensor() {

  


   //setting up memory stats
  byte i;
  byte present = 0;
  byte data[12];
  byte addr[8];

  //Basically below is the process of the program digging the sensor data from memory
  ds.reset_search();
  if ( !ds.search(addr)) {
      //Serial.print("No more addresses.\n");
      ds.reset_search();
      return;
  }

  ds.reset();
  ds.select(addr);
  ds.write(0x44,1);         // start conversion, with parasite power on at the end

  delay(1000);     // maybe 750ms is enough, maybe not
  // we might do a ds.depower() here, but the reset will take care of it.

  present = ds.reset();
  ds.select(addr);    
  ds.write(0xBE);         // Read Scratchpad
  
  for ( i = 0; i < 9; i++) {           // we need 9 bytes
    data[i] = ds.read();
    
  }

    //tempature data from the sensor
  int LowByte = data[0];
  int HighByte = data[1];
  int tempRead = (HighByte << 8) + LowByte;
  int SignBit = tempRead & 0x8000;  // test most sig bit
  if (SignBit) // negative
  {
    tempRead = (tempRead ^ 0xffff) + 1; // 2's comp
  }

  return tempRead;
  




 
}


void setupADC() {

  // ADC Multiplexer Selection Register
  ADMUX = 0;
  ADMUX |= (1 << REFS1); // Internal 2.56V Voltage Reference with external
                         // capacitor on AREF pin
  ADMUX |= (1 << REFS0); // Internal 2.56V Voltage Reference with external
                         // capacitor on AREF pin
  ADMUX |= (0 << MUX4);  // Temperature Sensor - 100111
  ADMUX |= (0 << MUX3);  // Temperature Sensor - 100111
  ADMUX |= (1 << MUX2);  // Temperature Sensor - 100111
  ADMUX |= (1 << MUX1);  // Temperature Sensor - 100111
  ADMUX |= (1 << MUX0);  // Temperature Sensor - 100111

  // ADC Control and Status Register A
  ADCSRA = 0;
  ADCSRA |= (1 << ADEN);  // Enable the ADC
  ADCSRA |= (1 << ADPS2); // ADC Prescaler - 16 (16MHz -> 1MHz)

  // ADC Control and Status Register B
  ADCSRB = 0;
  ADCSRB |= (1 << MUX5); // Temperature Sensor - 100111
}

int getTempComp() {
  ADCSRA |= (1 << ADSC); // Start temperature conversion
  while (bit_is_set(ADCSRA, ADSC))
    ; // Wait for conversion to finish
  byte low = ADCL;
  byte high = ADCH;
  int temperature = (high << 8) | low; // Result is in kelvin
  return temperature;            // change to calibrate
}

void setup() {
  pinMode(17, OUTPUT); // for LED
  pinMode(2, INPUT);
  
  
  Serial.begin(9600);
  

  Serial.println(F("Starting"));


  setupADC();
  // LMIC init
  os_init();
  // Reset the MAC state. Session and pending data transfers will be discarded.
  LMIC_reset();

// Set static session parameters. Instead of dynamically establishing a session
// by joining the network, precomputed session parameters are be provided.
#ifdef PROGMEM
  // On AVR, these values are stored in flash and only copied to RAM
  // once. Copy them to a temporary buffer here, LMIC_setSession will
  // copy them into a buffer of its own again.
  
//APB!
  uint8_t appskey[sizeof(APPSKEY)];
  uint8_t nwkskey[sizeof(NWKSKEY)];
  memcpy_P(appskey, APPSKEY, sizeof(APPSKEY));
  memcpy_P(nwkskey, NWKSKEY, sizeof(NWKSKEY));
  LMIC_setSession(0x1, DEVADDR, nwkskey, appskey);
  
#else
  // If not running an AVR with PROGMEM, just use the arrays directly
  LMIC_setSession(0x1, DEVADDR, NWKSKEY, APPSKEY);
#endif

#if defined(CFG_eu868)
  // Set up the channels used by the Things Network, which corresponds
  // to the defaults of most gateways. Without this, only three base
  // channels from the LoRaWAN specification are used, which certainly
  // works, so it is good for debugging, but can overload those
  // frequencies, so be sure to configure the full frequency range of
  // your network here (unless your network autoconfigures them).
  // Setting up channels should happen after LMIC_setSession, as that
  // configures the minimal channel set.
  // NA-US channels 0-71 are configured automatically
  LMIC_setupChannel(0, 868100000, DR_RANGE_MAP(DR_SF12, DR_SF7),
                    BAND_CENTI); // g-band
  LMIC_setupChannel(1, 868300000, DR_RANGE_MAP(DR_SF12, DR_SF7B),
                    BAND_CENTI); // g-band
  LMIC_setupChannel(2, 868500000, DR_RANGE_MAP(DR_SF12, DR_SF7),
                    BAND_CENTI); // g-band
  LMIC_setupChannel(3, 867100000, DR_RANGE_MAP(DR_SF12, DR_SF7),
                    BAND_CENTI); // g-band
  LMIC_setupChannel(4, 867300000, DR_RANGE_MAP(DR_SF12, DR_SF7),
                    BAND_CENTI); // g-band
  LMIC_setupChannel(5, 867500000, DR_RANGE_MAP(DR_SF12, DR_SF7),
                    BAND_CENTI); // g-band
  LMIC_setupChannel(6, 867700000, DR_RANGE_MAP(DR_SF12, DR_SF7),
                    BAND_CENTI); // g-band
  LMIC_setupChannel(7, 867900000, DR_RANGE_MAP(DR_SF12, DR_SF7),
                    BAND_CENTI); // g-band
  LMIC_setupChannel(8, 868800000, DR_RANGE_MAP(DR_FSK, DR_FSK),
                    BAND_MILLI); // g2-band
// TTN defines an additional channel at 869.525Mhz using SF9 for class B
// devices' ping slots. LMIC does not have an easy way to define set this
// frequency and support for class B is spotty and untested, so this
// frequency is not configured here.
#elif defined(CFG_us915)
  // NA-US channels 0-71 are configured automatically
  // but only one group of 8 should (a subband) should be active
  // TTN recommends the second sub band, 1 in a zero based count.
  // https://github.com/TheThingsNetwork/gateway-conf/blob/master/US-global_conf.json
  LMIC_selectSubBand(1);
#endif

  // Disable link check validation ABP
  LMIC_setLinkCheckMode(0);

  // TTN uses SF9 for its RX2 window.
  LMIC.dn2Dr = SF_Channel;

  // Set data rate and transmit power for uplink (note: txpow seems to be
  // ignored by the library)
  LMIC_setDrTxpow(SF_Channel, 14);

  // Start job
  do_send(&sendjob);
}

void loop() {
  digitalWrite(17, HIGH); // set the LED off
  TXLED1;
  
  os_runloop_once();
  
}
