#include <lmic.h>
//This file includes LoRaWAN keys and other config data so that they are easy to edit


// ABP!
// LoRaWAN NwkSKey, network session key
// ** FOR LIBRE DEVICES USING SAME NWKSKEY AND APPSKEY FOR ALL HW, DONT CHANGE
static const PROGMEM u1_t NWKSKEY[16] = { 0x22, 0x83, 0x0F, 0x78, 0xE2, 0xD1,
										  0xCA, 0x6E, 0x38, 0x21, 0x67, 0xB1,
										  0x31, 0x7D, 0x70, 0xD3 };

// LoRaWAN AppSKey, application session key
// ** FOR LIBRE DEVICES USING SAME NWKSKEY AND APPSKEY FOR ALL HW, DONT CHANGE
static const u1_t PROGMEM APPSKEY[16] = { 0xCE, 0x4B, 0x67, 0xCC, 0xE1, 0x8F,
										  0x51, 0x4E, 0xF7, 0x77, 0x07, 0x66,
										  0x57, 0x7F, 0x84, 0x0E };

// LoRaWAN end-device address (DevAddr)
static const u4_t DEVADDR = 0x26011FC6; 
// libre05 = 0x26011A76 / libre01 = 0x260113B9 / libre02 = 0x260112C7 / libre03 =
                // 0x26011008 / libre04 = 0x26011011



//FOR OTAA USAGE, DOESN'T WORK WITHOUT DOWNLINK BEING OPERATIONAL
/*
// This EUI must be in little-endian format, so least-significant-byte
// first. When copying an EUI from ttnctl output, this means to reverse
// the bytes. For TTN issued EUIs the last bytes should be 0xD5, 0xB3,
// 0x70.
static const u1_t PROGMEM APPEUI[8]={ 0x73, 0x92, 0x00, 0xD0, 0x7E, 0xD5, 0xB3, 0x70 };

void os_getArtEui (u1_t* buf) { memcpy_P(buf, APPEUI, 8);}

// This should also be in little endian format, see above.
static const u1_t PROGMEM DEVEUI[8]={ 0xEE, 0x60, 0x1A, 0x35, 0xC4, 0xD3, 0xF4, 0x00 };

void os_getDevEui (u1_t* buf) { memcpy_P(buf, DEVEUI, 8);}

// This key should be in big endian format (or, since it is not really a
// number but a block of memory, endianness does not really apply). In
// practice, a key taken from ttnctl can be copied as-is.
// The key shown here is the semtech default key.
static const u1_t PROGMEM APPKEY[16] = { 0xE6, 0x46, 0xC0, 0x0F, 0x11, 0x55, 0x25, 0xD9, 0x8D, 0x7C, 0x21, 0xA7, 0xDF, 0x28, 0x84, 0x0A };
void os_getDevKey (u1_t* buf) {  memcpy_P(buf, APPKEY, 16);}
*/


// Schedule TX every this many seconds (might become longer due to duty
// cycle limitations).
const unsigned TX_INTERVAL = 60; // 60 seconds, aka minute

//int tresshold = 480; //30c in raw data format
int tresshold2 = 30; //30c normally

period_t sleepTimer = SLEEP_8S; // The interval that is spend in sleep mode
//using enums to change the interval time is more or less required

OneWire  ds(2);  // So One Wire knows the pin its sensor is attached to 

dr_t SF_Channel = DR_SF7; // Define the SF channel used by the device

int messageType = 0x11; //only gives info in the message for the type of data being send
//aka, look up the the message types in the project wiki :L

// Pin mapping not particulary relevant, but if you are going to use them
// go for it ;P
const lmic_pinmap lmic_pins = {
    .nss = A3,
    .rxtx = LMIC_UNUSED_PIN,
    .rst = 8,
    .dio = {A2, A1, A0},
};